import { Table } from "./../types/Table";
import * as admin from "firebase-admin";
import * as functions from "firebase-functions";
import * as algoliasearch from "algoliasearch";

import firebaseConfigObj from "../../firebase.config";
import { appId, adminKey } from "../../algolia.config";
import { TABLES } from "./database";
import { User } from "../types/User";

admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: firebaseConfigObj.databaseURL
});

const db = admin.firestore();
const auth = admin.auth();
const messaging = admin.messaging();

// @ts-ignore
const client = algoliasearch(appId, adminKey);

["PROD", "DEV"].forEach(env => {
  const docEvent = `${env}_${TABLES.EVENTS}`;
  const docFeed = `${env}_${TABLES.FEED}`;

  const index = client.initIndex(`${docEvent}`.toLowerCase());

  exports[`handleWhenNewReservation${env}`] = functions.firestore
    .document(`${docEvent}/{eventId}/maps/{mapId}`)
    .onUpdate(async (change, context) => {
      const prevData = change.before.data();
      const newData = change.after.data();

      if (!newData || !prevData) {
        return new Error("Invalid data!");
      }

      const prevTablesReservations: Table[] = prevData.tables.filter(
        (table: Table) => table.reservation
      );

      const newTablesReservations: Table[] = newData.tables.filter(
        (table: Table) => table.reservation
      );

      // Reservation is added
      if (
        newTablesReservations.length > prevTablesReservations.length &&
        newTablesReservations.length - prevTablesReservations.length === 1
      ) {
        const prevReservations = prevTablesReservations.filter(
          table => table.reservation && table.id
        );
        const currReservations = newTablesReservations.filter(
          table => table.reservation && table.id
        );

        const differenceArray = currReservations.filter(
          (id, i) => id !== prevReservations[i]
        );

        // Get all the tokens from db and send push to them
        const tokensColl = await db.collection("fcm").get();
        const tokens = tokensColl.docs.map(doc => {
          if (doc.exists) {
            const data = doc.data();
            return data.token;
          }
        });

        if (!tokens.length) {
          return new Error("No subscribed users!");
        }

        if (!differenceArray.length) {
          return new Error("No new tables!");
        }

        console.log("[INFO] Found tokens in db " + tokens);
        const table = differenceArray[0];
        const { id, reservation } = table;

        if (!reservation) {
          return new Error("No reservation found!");
        }

        const { reservedBy, guestName, numberOfGuests } = reservation;

        if (!reservedBy || !guestName || !numberOfGuests) {
          return new Error("Reservation is invalid!");
        }

        const title = `New reservation on table ${id}`;
        const body = `${reservedBy.email} made a reservation for ${guestName}, ${numberOfGuests} pax.`;

        tokens.forEach(async token => {
          console.log("[INFO] Sending push to devices!");
          await messaging.sendToDevice(token, {
            notification: {
              title,
              body
            }
          });
        });

        // Add to Event Feed
        addToEventFeed(
          ChangeType.ADD,
          table,
          "Reservation",
          db.collection(`${docEvent}/${context.params.eventId}/eventFeed`)
        );

        // SEND CHANGES TO FEED
        return await db.collection(docFeed).add({
          timestamp: Date.now(),
          type: "Reservation",
          title,
          body
        });
      }

      return null;
    });

  exports[`addToIndex${env}`] = functions.firestore
    .document(`${docEvent}/{eventId}`)
    .onCreate(snapshot => {
      const data = snapshot.data();
      const objectID = snapshot.id;
      return index.addObject({ ...data, objectID });
    });

  exports[`updateIndex${env}`] = functions.firestore
    .document(`${docEvent}/{eventId}`)
    .onUpdate(change => {
      const newData = change.after.data();
      const objectID = change.after.id;
      return index.saveObject({ ...newData, objectID });
    });

  exports[`deleteIndex${env}`] = functions.firestore
    .document(`${docEvent}/{eventId}`)
    .onDelete(snapshot => index.deleteObject(snapshot.id));

  exports[`createUser${env}`] = functions.https.onCall(
    async ({ name, password, email, role, permissions }: User) => {
      try {
        const createdUser = await admin.auth().createUser({
          email,
          password
        });
        await auth.setCustomUserClaims(createdUser.uid, { role });
        return await db
          .collection(`${env}_${TABLES.USERS}`)
          .doc(createdUser.uid)
          .set({
            name,
            email,
            role,
            permissions
          });
      } catch (e) {
        return new Error(e);
      }
    }
  );
});

function addToEventFeed(
  change: ChangeType,
  table: Table,
  type: string,
  eventDoc: FirebaseFirestore.CollectionReference
) {
  let body;
  const { reservation, id } = table;
  switch (change) {
    case ChangeType.ADD:
      body = `${
        reservation!.reservedBy.email
      } made new reservation on table ${id}`;
      break;
    case ChangeType.DELETE:
      body = `${
        reservation!.reservedBy.email
      } deleted a reservation on table ${id}`;
      break;
    default:
      body = "";
  }
  return eventDoc.add({
    body,
    timestamp: Date.now(),
    type
  });
}

const enum ChangeType {
  DELETE = "delete",
  UPDATE = "update",
  ADD = "add"
}
