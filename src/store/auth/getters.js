import { Roles } from "../../config/roles";

export const isAdmin = (state) => {
    return state.user && state.user.role === Roles.ADMIN;
};

export const isLoggedIn = (state) => {
    return state.user && "email" in state.user;
};
