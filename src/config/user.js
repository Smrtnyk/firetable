import { Permissions } from "./permissions";
import { Roles } from "./roles";
export const User = Object.freeze({
    name: "",
    email: "",
    password: "",
    role: Object.values(Roles)[2],
    permissions: { ...Permissions }
});
