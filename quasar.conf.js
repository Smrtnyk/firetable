/* Configuration for your app
   https://quasar.dev/quasar-cli/quasar-conf-js */

module.exports = function (ctx) {
    return {
    /* App boot file (/src/boot)
       --> boot files are part of "main.js" */
        boot: ["serverConnection", "i18n", "filters"],

        css: ["app.styl"],

        extras: [
            /* 'ionicons-v4',
               'mdi-v3',
               'fontawesome-v5',
               'eva-icons', */
            "themify"
            // 'roboto-font-latin-ext', // this or either 'roboto-font', NEVER both!

            /* "roboto-font" // optional, you are not bound to it
               "material-icons-outlined" // optional, you are not bound to it */
        ],

        framework: {
            iconSet: "themify",
            // Lang: 'de', // Quasar language

            // All: true, // --- includes everything; for dev only!

            components: [
                "QLayout",
                "QHeader",
                "QDrawer",
                "QPageContainer",
                "QPage",
                "QPageSticky",
                "QToolbar",
                "QToolbarTitle",
                "QBtn",
                "QIcon",
                "QList",
                "QItem",
                "QItemSection",
                "QItemLabel",
                "QBanner",
                "QInput",
                "QSelect",
                "QForm",
                "QDate",
                "QSpace",
                "QAvatar",
                "QTooltip",
                "QImg",
                "QCard",
                "QCardSection",
                "QCardActions",
                "QDialog",
                "QSeparator",
                "QPopupProxy",
                "QInfiniteScroll",
                "QSpinnerDots",
                "QCarousel",
                "QCarouselSlide",
                "QCarouselControl",
                "QToggle",
                "QSlider",
                "QBadge",
                "QBtnGroup",
                "QCheckbox",
                "QChip",
                "QBtnToggle",
                "QParallax",
                "QColor",
                "QTimeline",
                "QTimelineEntry",
                "QExpansionItem"
            ],

            directives: ["Ripple", "ClosePopup"],

            // Quasar plugins
            plugins: ["Notify", "Dialog", "LocalStorage", "Loading", "BottomSheet"]
        },

        supportIE: false,

        build: {
            scopeHoisting: true,
            vueRouterMode: "history",
            // VueCompiler: true,
            gzip: true,
            /* Analyze: true,
               extractCSS: false, */
            extendWebpack (cfg) {
                cfg.module.rules.push({
                    enforce: "pre",
                    test: /\.(js|vue)$/,
                    loader: "eslint-loader",
                    exclude: /[\\/]node_modules[\\/]/,
                    options: { formatter: require("eslint").CLIEngine.getFormatter("stylish") }
                });
            },
            uglifyOptions: { compress: { drop_console: true } },
            env: {
                VUE_APP_FIREBASE_PROJECT_ID: JSON.stringify(
                    process.env.VUE_APP_FIREBASE_PROJECT_ID
                ),
                VUE_APP_FIREBASE_API_KEY: JSON.stringify(
                    process.env.VUE_APP_FIREBASE_API_KEY
                ),
                VUE_APP_MESSENGER_SENDER_ID: JSON.stringify(
                    process.env.VUE_APP_MESSENGER_SENDER_ID
                ),
                TABLE_PREFIX: JSON.stringify(process.env.TABLE_PREFIX),
                ALGOLIA_SEARCH: JSON.stringify(process.env.ALGOLIA_SEARCH),
                ALGOLIA_ADMIN: JSON.stringify(process.env.ALGOLIA_ADMIN),
                ALGOLIA_ID: JSON.stringify(process.env.ALGOLIA_ID)
            }
        },

        devServer: {
            https: true,
            port: 8080,
            open: true // Opens browser window automatically
        },

        // Animations: 'all', // --- includes all animations
        animations: [],

        ssr: { pwa: false },

        pwa: {
            workboxPluginMode: "InjectManifest",
            // WorkboxOptions: {}, // only for NON InjectManifest
            manifest: {
                name: "Firetable",
                short_name: "Firetable",
                description: "Firetable reservation management system",
                display: "standalone",
                orientation: "portrait",
                background_color: "#ffffff",
                theme_color: "#027be3",
                icons: [
                    {
                        src: "statics/icons/icon-128x128.png",
                        sizes: "128x128",
                        type: "image/png"
                    },
                    {
                        src: "statics/icons/icon-192x192.png",
                        sizes: "192x192",
                        type: "image/png"
                    },
                    {
                        src: "statics/icons/icon-256x256.png",
                        sizes: "256x256",
                        type: "image/png"
                    },
                    {
                        src: "statics/icons/icon-384x384.png",
                        sizes: "384x384",
                        type: "image/png"
                    },
                    {
                        src: "statics/icons/icon-512x512.png",
                        sizes: "512x512",
                        type: "image/png"
                    }
                ]
            }
        },

        cordova: {
            /* Id: 'firetable.app',
               noIosLegacyBuildFlag: true, // uncomment only if you know what you are doing */
        },

        electron: {
            // Bundler: 'builder', // or 'packager'

            extendWebpack (cfg) {
                /* Do something with Electron main process Webpack cfg
                   chainWebpack also available besides this extendWebpack */
            },

            packager: {
                /* https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options
                   OS X / Mac App Store
                   appBundleId: '',
                   appCategoryType: '',
                   osxSign: '',
                   protocol: 'myapp://path',
                   Windows only
                   win32metadata: { ... } */
            },

            builder: {
                /* https://www.electron.build/configuration/configuration
                   appId: 'firetable' */
            }
        }
    };
};
