import Vue from "vue";
import Vuex from "vuex";
import { vuexfireMutations } from "vuexfire";
import { showErrorMessage } from "../helpers/UI-helpers";

import auth from "./auth";
import places from "./store-places";
import events from "./events";
import settings from "./store-settings";
import feed from "./store-feed";

Vue.use(Vuex);

const actions = {
    async init ({ dispatch }) {
        try {
            await Promise.all([
                dispatch("places/initPlaces", null, { root: true }),
                dispatch("settings/initDefaultMap", null, { root: true }),
                dispatch("events/initSpecialEvents", null, { root: true }),
                dispatch("feed/initFeed", null, { root: true }),
                dispatch("auth/addMessagingToken", null, { root: true })
            ]);
        } catch (err) {
            showErrorMessage(err.message);
        }
    }
};

export default new Vuex.Store({
    mutations: { ...vuexfireMutations },
    actions,
    modules: {
        auth,
        places,
        events,
        settings,
        feed
    },
    strict: process.env.DEV
});
