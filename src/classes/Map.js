import d3 from "../lib/d3";
import { SelectionRect } from "./SelectionRect";

/**
 * Config object for different editor modes
 * EDITOR - When in a map preset editor
 * SINGLE - When in a single event map editor
 * LIVE   - When in a event page where reservations ocurr
 */
export const MODES = Object.freeze({
    EDITOR: "EDITOR",
    SINGLE: "SINGLE",
    LIVE: "LIVE"
});

export const TABLE_MODES = Object.freeze({
    STANDARD: "STANDARD",
    WIDE: "WIDE",
    TALL: "TALL",
    ROUND: "ROUND"
});

/**
 * Constant for table width in pixels
 */
const TABLE_WIDTH = 30;

/**
 * Constant for table height in pixels
 */
const TABLE_HEIGHT = 30;

/**
 * Constant for defining how wide is border around svg map
 */
const BORDER_WIDTH = 1;

/**
 * Constant for snap value when dragging the table
 */
const RESOLUTION = 10;

/**
 * Constant for svg width
 */
const MAP_WIDTH = 400;

/**
 * Constant for svg height
 */
const MAP_HEIGHT = 650;

class Map {
    /**
   * Constructor function that instantiates new Map object
   * el - HTMLElement to append svg map to it
   * data - Array of objects with tables data or empty array
   * isEditor - Boolean value to specify if is in editor mode or not
   */
    constructor ({
        id,
        name = "New Region",
        width = MAP_WIDTH,
        height = MAP_HEIGHT,
        data = [],
        mode = MODES.LIVE,
        tableClickHandler,
        dblClickHandler
    }) {
        this._id = id;
        this.name = name;
        this._data = data;
        this.width = parseInt(width, 10);
        this.height = parseInt(height, 10);
        this.mode = mode;
        this.onTableClickHandler = tableClickHandler;
        this.dblClickHandler = dblClickHandler;
    }

    set el (newEl) {
        this._el = newEl;
    }

    get el () {
        return this._el;
    }

    get id () {
        return this._id;
    }

    get data () {
        return this._data;
    }

    get name () {
        return this._name;
    }

    set name (newName) {
        this._name = newName;
    }

    set data (newData) {
        this._data = newData;
    }

    /**
   * Restart method to redraw the map when data is changed
   */
    restart () {
        if (this.svg) {
            this.svg.remove();
        }

        this.svg = d3
            .select(this.el)
            .append("svg")
            .attr("viewBox", `0 0 ${this.width} ${this.height}`)
            .attr("preserveAspectRatio", "xMidYMid")
            .attr("class", `MapEditor__SVG ${this.name}`);

        if (this.mode === MODES.EDITOR) {
            this.selectionRect = new SelectionRect(this.svg);
            this.selectionRect.initialize();
            this.setupEditor();
        }

        let group = this.svg.selectAll("g.tableGroup").data(this.data, (d) => {
            return d.id;
        });

        group.exit().remove();

        const circleEnter = group
            .enter()
            .filter((d) => {
                return d.type === TABLE_MODES.ROUND;
            })
            .append("g")
            .attr("class", "tableGroup");

        const enter = group
            .enter()
            .filter((d) => {
                return d.type !== TABLE_MODES.ROUND;
            })
            .append("g")
            .attr("class", "tableGroup");

        circleEnter
            .append("circle")
            .attr("cx", (d) => {
                return d.x;
            })
            .attr("cy", (d) => {
                return d.y;
            })
            .attr("r", TABLE_WIDTH / 1.85)
            .style("stroke", "#111");

        enter
            .append("rect")
            .attr("x", (d) => {
                return d.x;
            })
            .attr("y", (d) => {
                return d.y;
            })
            .attr("height", (d) => {
                return d.type === TABLE_MODES.TALL ? TABLE_HEIGHT + 10 : TABLE_HEIGHT;
            }
            )
            .attr("width", (d) => {
                return d.type === TABLE_MODES.WIDE ? TABLE_WIDTH + 10 : TABLE_WIDTH;
            }
            )
            .style("stroke", "#111");

        circleEnter
            .append("text")
            .attr("x", (d) => {
                return d.x;
            })
            .attr("y", (d) => {
                return d.y + 5;
            })
            .attr("class", "table__id")
            .text((d) => {
                return d.id;
            });

        enter
            .append("text")
            .attr("x", (d) => {
                return d.x + 15;
            })
            .attr("y", (d) => {
                return d.y + 19;
            })
            .attr("class", (d) => {
                return `table__id table__id__${d.id}`;
            })
            .text((d) => {
                return d.id;
            });

        if (this.mode === MODES.EDITOR) {
            circleEnter.call(
                d3
                    .drag()
                    .on("start", this.dragstarted)
                    .on("drag", this.dragged(this))
                    .on("end", this.dragended)
            );
            enter.call(
                d3
                    .drag()
                    .on("start", this.dragstarted)
                    .on("drag", this.dragged(this))
                    .on("end", this.dragended)
            );
        }

        circleEnter
            .on("click", (d) => {
                d3.event.stopPropagation();
                this.onTableClickHandler(this, d);
            })
            .selectAll("circle")
            .style("fill", (d) => {
                return d.reservation ? "#2ab7ca" : "#333";
            })
            .attr("class", (d) => {
                let className = "";
                if (d.reservation) {
                    className += "reserved";
                }
                if (d.reservation && d.reservation.came) {
                    className += " came";
                }
                className += ` table__${d.id}`;
                return className;
            });

        enter
            .on("click", (d) => {
                d3.event.stopPropagation();
                this.onTableClickHandler(this, d);
            })
            .selectAll("rect")
            .style("fill", (d) => {
                return d.reservation ? "#2ab7ca" : "#333";
            })
            .attr("class", (d) => {
                let className = "";
                if (d.reservation) {
                    className += "reserved";
                }
                if (d.reservation && d.reservation.came) {
                    className += " came";
                }
                className += ` table__${d.id}`;
                return className;
            });

        group = group.merge(enter);
        group = group.merge(circleEnter);

        group.selectAll("text").text((d) => {
            return d.id;
        });
    }

    dragstarted (d) {
        d3.select(this).classed("active", true);
    }

    dragended (d) {
        d3.select(this).classed("active", false);
    }

    /**
   * Drag handler for table
   */
    dragged (self) {
        return function (d) {
            function round (p, n) {
                return p % n < n / 2 ? p - p % n : p + n - p % n;
            }
            const gridX = round(
                Math.max(3, Math.min(self.width - TABLE_WIDTH, d3.event.x)),
                RESOLUTION
            );
            const gridY = round(
                Math.max(3, Math.min(self.height - TABLE_HEIGHT, d3.event.y)),
                RESOLUTION
            );

            if (d.type === TABLE_MODES.ROUND) {
                d3.select(this)
                    .select("circle")
                    .attr("cx", d.x = gridX)
                    .attr("cy", d.y = gridY);

                d3.select(this)
                    .select("text")
                    .attr("x", d.x = gridX)
                    .attr("y", d.y = gridY + 5);

                return;
            }
            d3.select(this)
                .select("text")
                .attr("x", d.x = gridX + 15)
                .attr("y", d.y = gridY + 19);

            d3.select(this)
                .select("rect")
                .attr("x", d.x = gridX)
                .attr("y", d.y = gridY);
        };
    }

    setupEditor () {
        this.svg
            .selectAll(".vertical")
            .data(d3.range(1, this.width / RESOLUTION))
            .enter()
            .append("line")
            .attr("class", "vertical")
            .attr("x1", (d) => {
                return d * RESOLUTION;
            })
            .attr("y1", 0)
            .attr("x2", (d) => {
                return d * RESOLUTION;
            })
            .attr("y2", this.height);

        this.svg
            .selectAll(".horizontal")
            .data(d3.range(1, this.height / RESOLUTION))
            .enter()
            .append("line")
            .attr("class", "horizontal")
            .attr("x1", 0)
            .attr("y1", (d) => {
                return d * RESOLUTION;
            })
            .attr("x2", this.width)
            .attr("y2", (d) => {
                return d * RESOLUTION;
            });
    }

    /**
   *
   */
    init () {
    // App starts here
        this.restart();
        if (this.mode === MODES.EDITOR) {
            this.svg.on("dblclick", () => {
                d3.event.preventDefault();
                this.dblClickHandler(this, d3.mouse(d3.event.path[0]));
            });
            /* D3.select(window);
                  .on("keydown", keydown)
                  .on("keyup", keyup);
               .on("mousedown", mousedown)
               .on("mouseup", mouseup);
               this.restart(); */
        }
    }

    /**
   * Static build method to return instance of map for region
   */
    static buildRegion (paramObj) {
        return new Map(paramObj);
    }
}

export default Map;
