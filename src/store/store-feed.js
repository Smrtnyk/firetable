import { Loading } from "quasar";
import { feed } from "../services/firebase/db";
import { firestoreAction } from "vuexfire";
import { showErrorMessage } from "../helpers/UI-helpers";

const state = { feed: [] };

const getters = {};

const actions = {
    initFeed: firestoreAction(async ({ bindFirestoreRef }) => {
        Loading.show();
        await bindFirestoreRef("feed", feed().limit(10)).catch(
            showErrorMessage
        );
        Loading.hide();
    })
};

export default {
    namespaced: true,
    state,
    getters,
    actions
};
