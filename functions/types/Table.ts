import { User } from "./User";

export interface Table {
  id: string;
  region: string;
  reservation?: Reservation;
  type: TABLE_MODE;
  x: number;
  y: number;
}

export interface Reservation {
  came: boolean;
  groupedWith: string[];
  guestContact?: string;
  guestName: string;
  numberOfGuests: number;
  reservationNote?: string;
  reservedBy: User;
}

const enum TABLE_MODE {
  STANDARD = "STANDARD",
  WIDE = "WIDE",
  TALL = "TALL",
  ROUND = "ROUND"
}
