export const Permissions = Object.freeze({
    canReserveAll: true,
    canDeleteAll: true
});
