import { storage } from "./base";

const getRef = () => {
    return storage().ref();
};

export const uploadFile = async ({
    destination, base64, type
}) => {
    const fileRef = getRef().child(destination);
    const url = await fileRef.putString(base64, type);
    return url.ref.getDownloadURL();
};

export const deleteFile = ({
    folder, filename
}) => {
    return getRef()
        .child(`${folder}/${encodeURIComponent(filename)}`)
        .delete();
};
