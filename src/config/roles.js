// Roles Enum
export const Roles = Object.freeze({
    ADMIN: "Administrator",
    MANAGER: "Manager",
    WAITER: "Waiter",
    ENTRY: "Entry"
});
