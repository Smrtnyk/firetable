import { Dialog, Notify } from "quasar";

export const notifyPositive = (message) => {
    Notify.create({
        color: "green-4",
        textColor: "white",
        icon: "ti-check",
        message
    });
};

export const showConfirm = (title) => {
    return new Promise((resolve, _) => {
        Dialog.create({
            title,
            message: "Confirm delete?",
            cancel: true,
            persistent: true
        })
            .onOk(() => {
                resolve(true);
            })
            .onCancel(() => {
                resolve(false);
            });
    });
};

export const showErrorMessage = (e) => {
    Dialog.create({
        title: "Error",
        message: e.message ? e.message : e,
        class: "error-dialog"
    });
};
