import { firestoreAction } from "vuexfire";
import { users } from "../../services/firebase/db";
import { showErrorMessage } from "../../helpers/UI-helpers";

export const createNewUser = function (_, payload) {
    const $fb = this.$fb;
    return $fb.createUserWithEmail(payload);
};

export const loginUser = function (_, payload) {
    const $fb = this.$fb;
    const {
        email, password
    } = payload;
    return $fb.loginWithEmail(email, password);
};

export const logoutUser = async function () {
    const $fb = this.$fb;
    await $fb.logoutUser();
};

export function routeUserToAuth () {
    this.$router.push({ path: "/auth" });
}

export const initUser = firestoreAction(({ bindFirestoreRef }, uid) => {
    return bindFirestoreRef("user", users().doc(uid)).catch(showErrorMessage);
}
);

export const initUsers = firestoreAction(({ bindFirestoreRef }) => {
    return bindFirestoreRef("users", users()).catch(showErrorMessage);
}
);

export const addMessagingToken = async function ({ state }) {
    const $fb = this.$fb;
    await $fb.addMessagingToken(state.user.email);
};

export const removeMessagingToken = async function ({ state }) {
    const $fb = this.$fb;
    await $fb.removeMessagingToken(state.user.email);
};
