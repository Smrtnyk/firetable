import enUS from "./en-us";
import srRs from "./sr-RS";

export default {
    "en-us": enUS,
    "sr-RS": srRs
};
