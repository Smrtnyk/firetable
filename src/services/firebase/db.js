import { firestore } from "./base";

export const TABLES = {
    EVENTS: "events",
    GUEST_LIST: "guestList",
    USERS: "users",
    SETTINGS: "settings",
    FEED: "feed",
    PLACES: "places",
    FCM: "fcm"
};

function collection (table) {
    return firestore().collection(process.env.TABLE_PREFIX + table);
}

// TABLES
export function events () {
    return collection(TABLES.EVENTS);
}

export function guestList (eventTable) {
    return collection(TABLES.EVENTS).doc(eventTable).collection(TABLES.GUEST_LIST);
}

export function feed () {
    return collection(TABLES.FEED);
}

export function users () {
    return collection(TABLES.USERS);
}

export function settings () {
    return collection(TABLES.SETTINGS);
}

export function places () {
    return collection(TABLES.PLACES);
}

export function fcm () {
    return collection(TABLES.FCM);
}
