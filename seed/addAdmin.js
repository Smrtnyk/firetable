const admin = require("firebase-admin");
const serviceAccount = require("../serviceAccount.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://firetable-eu.firebaseio.com"
});

const db = admin.firestore();
const auth = admin.auth();

const addAdmin = async () => {
  try {
    const user = await auth.createUser({
      email: "admindev@firetable.at",
      password: "admin123"
    });

    console.log(user.uid);

    await auth
      .setCustomUserClaims(user.uid, { role: "Administrator" })
      .then(() => {
        //Interesting to note: we need to re-fetch the userRecord, as the user variable **does not** hold the claim
        return admin.auth().getUser(user.uid);
      })
      .then(userRecord => {
        console.log(userRecord.uid);
        console.log(userRecord.customClaims.role);
        return null;
      });

    await db
      .collection("DEV_users")
      .doc(user.uid)
      .set({
        email: "admindev@firetable.at",
        name: "Admin",
        role: "Administrator",
        region: "all",
        personalEmail: null,
        address: null,
        mobile: null,
        atWork: false,
        avatar: "https://ui-avatars.com/api/?name=Admin",
        points: 0
      });

    process.exit();
  } catch (err) {
    console.log(err);
    process.exit();
  }
};

addAdmin();
