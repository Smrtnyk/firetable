import { firestoreAction } from "vuexfire";
import { settings } from "../services/firebase/db";
import { showErrorMessage } from "../helpers/UI-helpers";

const state = { defaultMap: [] };

const getters = {};

const mutations = {};

const actions = {
    initDefaultMap: firestoreAction(({ bindFirestoreRef }) => {
        return bindFirestoreRef(
            "defaultMap",
            settings()
                .doc("defaultMap")
                .collection("regions")
        ).catch(showErrorMessage);
    }),

    saveDefaultMap (_, {
        id, name, data, width, height
    }) {
        return settings()
            .doc("defaultMap")
            .collection("regions")
            .doc(id)
            .set({
                id,
                name,
                tables: data,
                width,
                height
            });
    },
    getDefaultMap () {
        return settings()
            .doc("defaultMap")
            .collection("regions")
            .get();
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
};
