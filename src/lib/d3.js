import { event, mouse, select, selectAll } from "d3-selection";
import { range } from "d3-array";
import { drag } from "d3-drag";

export default {
    select,
    selectAll,
    get event () {
        return event;
    },
    mouse,
    range,
    drag
};
