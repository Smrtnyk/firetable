import { auth, functions } from "./base";

/**
 * Https: //firebase.google.com/docs/reference/js/firebase.auth.Auth.html#create-user-with-email-and-password
 *
 * @param {String} email - A Valid email
 * @param {String} password - Password
 *
 * @return {Promise} UserCredentials
 */
export const createUserWithEmail = async (payload) => {
    const newUser = functions().httpsCallable(
        `createUser${process.env.TABLE_PREFIX.replace("_", "")}`
    );
    const createdUser = await newUser(payload);
    return createdUser;
};

/**
 * Remove firebase auth token
 */
export const logoutUser = () => {
    return auth().signOut();
};

/**
 * @param {String} email - A Valid email
 * @param {String} password - Password
 *
 * @return {Promise} UserCredentials
 */
export const loginWithEmail = (email, password) => {
    return auth().signInWithEmailAndPassword(email, password);
};
