export const messages = Object.freeze({
    "no-special-events": "No special events, go and create one!",
    "special-event-created": "Event created successfuly!"
});
