import { initializeApp } from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";
import "firebase/firebase-messaging";
import "firebase/firebase-functions";

import { Notify } from "quasar";
import { Roles } from "../../config/roles";

let app;

export function getFirebaseApp () {
    return app;
}

function setFirebaseApp (appObj) {
    app = appObj;
}

export function storage () {
    return getFirebaseApp().storage();
}

export function functions () {
    return getFirebaseApp().functions();
}

export function messaging () {
    return getFirebaseApp().messaging();
}

/**
 * Firebase's auth interfact method
 * https: //firebase.google.com/docs/reference/js/firebase.auth.html#callable
 * @return {Object} currentUser object from firebase
 */
export function auth () {
    return getFirebaseApp().auth();
}

/**
 * Firestore
 * https: //firebase.google.com/docs/reference/js/firebase.firestore.Firestore.html
 *
 * @return {Interface} returns Firestore
 */
export function firestore () {
    return getFirebaseApp().firestore();
}

/**
 * Async function providing the application time to
 * wait for firebase to initialize and determine if a
 * user is authenticated or not with only a single observable
 */
export function ensureAuthIsInitialized (store) {
    if (store.state.auth.isReady) {
        return true;
    }
    // Create the observer only once on init
    return new Promise((resolve, reject) => {
    // Use a promise to make sure that the router will eventually show the route after the auth is initialized.
        const unsubscribe = auth().onAuthStateChanged(
            (user) => {
                resolve();
                unsubscribe();
            },
            () => {
                reject(
                    new Error(
                        "Looks like there is a problem with the firebase service. Please try again later"
                    )
                );
            }
        );
    });
}

/**
 * Convenience method to initialize firebase app
 *
 * @param  {Object} config
 */
export function fBInit (config) {
    setFirebaseApp(initializeApp(config));
}

/**
 * @param  {Object} store - Vuex store
 */
export function isAuthenticated (store) {
    return store.state.auth.isAuthenticated;
}

/** Handle the auth state of the user and
 * set it in the auth store module
 * @param  {Object} store - Vuex Store
 * @param  {Object} currentUser - Firebase currentUser
 */
export async function handleOnAuthStateChanged (store, currentUser) {
    const initialAuthState = isAuthenticated(store);
    // Save to the store
    store.commit("auth/setAuthState", {
        isAuthenticated: currentUser !== null,
        isReady: true
    });

    if (currentUser) {
        await store.dispatch("auth/initUser", currentUser.uid);
        await store.dispatch("init");
    }

    /* If the user loses authentication route
    /* redirect them to the login page */
    if (!currentUser && initialAuthState) {
        store.dispatch("auth/routeUserToAuth");
    }
}

/**
 * @param  {Object} router - Vue Router
 * @param  {Object} store - Vuex Store
 */
export function routerBeforeEach (router, store) {
    router.beforeEach(async (to, from, next) => {
        try {
            /* Force the app to wait until Firebase has
               Finished its initialization, and handle the
               Authentication state of the user properly */
            await ensureAuthIsInitialized(store);

            const requiresAuth = to.matched.some((record) => {
                return record.meta.requiresAuth;
            });
            const requiresAdmin = to.matched.some(
                (record) => {
                    return record.meta.requiresAdmin;
                }
            );

            if (requiresAuth && !isAuthenticated(store)) {
                next({ path: "/auth" });
                return;
            }

            if (isAuthenticated(store)) {
                const token = await auth().currentUser.getIdTokenResult();
                const role = token && token.claims && token.claims.role;
                const isAdmin = role === Roles.ADMIN;

                if (requiresAdmin && !isAdmin) {
                    next("/");
                } else if (requiresAdmin && isAdmin) {
                    next();
                } else if (to.path === "/auth") {
                    next("/");
                } else {
                    next();
                }
            } else {
                next();
            }
        } catch (err) {
            Notify.create({
                message: `${err}`,
                color: "negative"
            });
        }
    });
}
