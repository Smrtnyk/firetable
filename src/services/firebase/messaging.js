import { messaging } from "./base";
import { fcm } from "./db";

export const addMessagingToken = async (email) => {
    try {
        const token = await messaging().getToken();
        if (token) {
            return fcm()
                .doc(email)
                .set({ token });
        }
    } catch (e) {
        // TODO: Log here
    }
};

export const deleteMessagingToken = (email) => {
    return fcm()
        .doc(email)
        .delete();
};
