export const upcomingEvents = (state) => {
    return [...state.events].splice(0, 4);
};
