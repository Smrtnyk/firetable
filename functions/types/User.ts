export const enum Role {
  ADMIN = "Administrator",
  MANAGER = "Manager",
  WAITER = "Waiter",
  ENTRY = "Entry"
}
export interface Permissions {
  [key: string]: boolean;
}
export interface User {
  name: string;
  email: string;
  password: string;
  role: Role;
  permissions: Permissions;
}
