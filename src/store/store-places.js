import { firestoreAction } from "vuexfire";
import { places } from "../services/firebase/db";
import { showErrorMessage } from "../helpers/UI-helpers";

const state = {
    places: [],
    place: null
};

const getters = {};

const actions = {
    initPlaces: firestoreAction(({ bindFirestoreRef }) => {
        return bindFirestoreRef("places", places()).catch(showErrorMessage);
    }
    ),

    initPlace: firestoreAction(({ bindFirestoreRef }, id) => {
        return bindFirestoreRef("place", places().doc(id)).catch(showErrorMessage);
    })
};

export default {
    namespaced: true,
    state,
    getters,
    actions
};
