import * as base from "./base";
import * as db from "./db";
import * as storage from "./storage";
import * as messaging from "./messaging";

import * as email from "./email";
import * as dbEvents from "./db-events";

export default Object.assign({}, base, db, storage, email, messaging, dbEvents);
