import d3 from "../lib/d3";

export class SelectionRect {
    constructor (svg) {
        this.svg = svg;
        this.element = null;
        this.previousElement = null;
        this.currentY = 0;
        this.currentX = 0;
        this.originX = 0;
        this.originY = 0;
        this.dragBehavior = d3
            .drag()
            .on("drag", this.dragMove(this))
            .on("start", this.dragStart(this))
            .on("end", this.dragEnd(this));
    }

    initialize () {
        this.svg.call(this.dragBehavior);
    }

    setElement (ele) {
        this.previousElement = this.element;
        this.element = ele;
    }

    getNewAttributes () {
        const x = this.currentX < this.originX ? this.currentX : this.originX;
        const y = this.currentY < this.originY ? this.currentY : this.originY;
        const width = Math.abs(this.currentX - this.originX);
        const height = Math.abs(this.currentY - this.originY);
        return {
            x,
            y,
            width,
            height
        };
    }

    getCurrentAttributes () {
    // Use plus sign to convert string into number
        const x = +this.element.attr("x");
        const y = +this.element.attr("y");
        const width = +this.element.attr("width");
        const height = +this.element.attr("height");
        return {
            x1: x,
            y1: y,
            x2: x + width,
            y2: y + height
        };
    }

    getCurrentAttributesAsText () {
        const attrs = this.getCurrentAttributes();
        return (
            "x1: " +
      attrs.x1 +
      " x2: " +
      attrs.x2 +
      " y1: " +
      attrs.y1 +
      " y2: " +
      attrs.y2
        );
    }

    init (newX, newY) {
        const rectElement = this.svg
            .append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", 0)
            .attr("height", 0)
            .classed("selection", true);
        this.setElement(rectElement);
        this.originX = newX;
        this.originY = newY;
        this.update(newX, newY);
    }

    update (newX, newY) {
        this.currentX = newX;
        this.currentY = newY;
        const {
            x, y, width, height
        } = this.getNewAttributes();
        this.element
            .attr("x", x)
            .attr("y", y)
            .attr("width", width)
            .attr("height", height);
    }

    focus () {
        this.element.style("stroke", "#DE695B").style("stroke-width", "2.5");
    }

    remove () {
        this.element.remove();
        this.element = null;
    }

    removePrevious () {
        if (this.previousElement) {
            this.previousElement.remove();
        }
    }

    dragStart (self) {
        return function () {
            const [x, y] = d3.mouse(this);
            self.init(x, y);
            self.removePrevious();
        };
    }

    dragMove (self) {
        return function () {
            const [x, y] = d3.mouse(this);
            self.update(x, y);
        };
    }

    dragEnd (self) {
        return function () {
            const finalAttributes = self.getCurrentAttributes();
            if (
                finalAttributes.x2 - finalAttributes.x1 > 1 &&
        finalAttributes.y2 - finalAttributes.y1 > 1
            ) {
                // D3.event.preventDefault();
                self.focus();
            } else {
                // Single point selected
                self.remove();
            }
        };
    }
}
