import firebaseService from "../services/firebase";
import { showErrorMessage } from "../helpers/UI-helpers";

export default ({
    router, store, Vue
}) => {
    firebaseService.fBInit({
        appId: "1:604176749699:web:cc48a2a03165a526",
        apiKey: process.env.VUE_APP_FIREBASE_API_KEY,
        authDomain: `${process.env.VUE_APP_FIREBASE_PROJECT_ID}.firebaseapp.com`,
        databaseURL: `https://${process.env.VUE_APP_FIREBASE_PROJECT_ID}.firebaseio.com`,
        projectId: process.env.VUE_APP_FIREBASE_PROJECT_ID,
        storageBucket: `${process.env.VUE_APP_FIREBASE_PROJECT_ID}.appspot.com`,
        messagingSenderId: process.env.VUE_APP_MESSENGER_SENDER_ID
    });

    /* Tell the application what to do when the
       authentication state has changed */
    firebaseService.auth().onAuthStateChanged(
        (user) => {
            firebaseService.handleOnAuthStateChanged(store, user);
        },
        (error) => {
            showErrorMessage(error);
        }
    );

    /* Setup the router to be intercepted on each route.
       This allows the application to halt rendering until
       Firebase is finished with its initialization process,
       and handle the user accordingly */
    firebaseService.routerBeforeEach(router, store);

    Vue.prototype.$fb = firebaseService;
    store.$fb = firebaseService;
};
