export const TABLES = {
  EVENTS: "events",
  USERS: "users",
  SETTINGS: "settings",
  FEED: "feed",
  PLACES: "places",
  FCM: "fcm"
};
