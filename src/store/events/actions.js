import { firestoreAction } from "vuexfire";
import { events } from "../../services/firebase/db";
import { showErrorMessage } from "../../helpers/UI-helpers";
import { Loading, Notify } from "quasar";
import { i18n } from "../../boot/i18n";

export const initSpecialEvents = firestoreAction(({ bindFirestoreRef }) => {
    return bindFirestoreRef(
        "events",
        events()
            .where("eventDate", ">=", new Date())
            .where("daily", "==", false)
            .orderBy("eventDate")
            .limit(30)
    ).catch(showErrorMessage);
});

export const initAdminSpecialEvents = firestoreAction(
    ({ bindFirestoreRef }) => {
        return bindFirestoreRef(
            "adminSpecialEvents",
            events()
                .where("daily", "==", false)
                .orderBy("eventDate", "desc")
                .limit(30)
        ).catch(showErrorMessage);
    }
);

export const initDailyEvents = firestoreAction(({ bindFirestoreRef }) => {
    return bindFirestoreRef(
        "events",
        events().where("eventDate", ">=", new Date())
    ).catch(showErrorMessage);
});

export const initEvent = firestoreAction(({ bindFirestoreRef }, id) => {
    return bindFirestoreRef("event", events().doc(id)).catch(showErrorMessage);
});

/**
 * Initialize guest list collection for specified event
 */
export const initGuestList = firestoreAction(({ bindFirestoreRef }, eventID) => {
    return bindFirestoreRef("guestList", events().doc(eventID).collection("guestList"));
});

export const initEventMaps = firestoreAction(({ bindFirestoreRef }, id) => {
    return bindFirestoreRef(
        "eventMaps",
        events()
            .doc(id)
            .collection("maps")
    ).catch(showErrorMessage);
});

/**
 * Action for creating a special event and saving it to database
 */
export const createEvent = async function ({ rootState }, eventPayload) {
    const $fb = this.$fb;
    Loading.show();

    try {
        await $fb.createNewEvent(rootState, eventPayload);
        Notify.create(i18n.t("special-event-created"));
    } catch (e) {
        showErrorMessage(e);
    }

    Loading.hide();
};

/**
 * Action for adding a reservation to a table and saving it to database
 */
export const addReservation = async function (
    { rootState },
    reservationPayload
) {
    Loading.show();
    try {
        const $fb = this.$fb;
        await $fb.addReservation(rootState, reservationPayload);
        Notify.create({
            color: "green-4",
            textColor: "white",
            icon: "ti-check",
            message: "Table reserved"
        });
    } catch (e) {
        showErrorMessage(e);
    }
    Loading.hide();
};

/**
 * Action for deleting a particular reservation from a table and adding it to database
 */
export const deleteReservation = (
    _,
    {
        eventId, region, reservation
    }
) => {
    const { groupedWith } = reservation;
    const tables = [...region.data];

    groupedWith.forEach((id) => {
        const findReservationToDelete = tables.find((table) => {
            return table.id === id;
        });
        if (findReservationToDelete) {
            delete findReservationToDelete.reservation;
        }
    });

    return events()
        .doc(eventId)
        .collection("maps")
        .doc(region.id)
        .update({ tables });
};

export const updateReservation = (
    _,
    {
        val, region, prop, eventId, reservation
    }
) => {
    const newTables = region.data;
    const { groupedWith } = reservation;

    groupedWith.forEach((id) => {
        const reservationIndex = newTables.findIndex((table) => {
            return table.id === id;
        });
        if (reservationIndex !== -1) {
            const newReservation = JSON.parse(
                JSON.stringify(newTables[reservationIndex])
            );
            newReservation.reservation[prop] = val;
            newTables[reservationIndex] = newReservation;
        }
    });

    return events()
        .doc(eventId)
        .collection("maps")
        .doc(region.id)
        .update({ tables: newTables });
};

/**
 * Get Event method
 */
export const getEvent = async ({ commit }, id) => {
    Loading.show();
    try {
        const event = await events()
            .doc(id)
            .get();

        if (event.exists) {
            Loading.hide();
            return event.data();
        }

        return this.$router.go(-1);
    } catch (e) {
        Loading.hide();
        showErrorMessage(e);
    }
};

/**
 * Delete Event method
 */
export const deleteEvent = async (_, event) => {
    Loading.show();

    try {
        const {
            img, id
        } = event;
        await events()
            .doc(id)
            .delete();
        if (img) {
            // TODO: Fix this ImageUploader shit
            await ImageUploader.delete("events", id);
        }
        this.$router.replace("/admin/events");
    } catch (e) {
        showErrorMessage(e);
    }

    Loading.hide();
};


/**
 * Action to add guest to specified guestlist.
 */
export async function addGuestToGuestList (_, {
    eventID, payload
}) {
    Loading.show();
    try {
        await this.$fb.addGuestToGuestList(eventID, payload);
    } catch (e) {
        showErrorMessage(e);
    }
    Loading.hide();
}

/**
 * Action to delete guest from specified guestlist.
 */
export async function deleteGuestFromGuestList (_, {
    eventID, guestID
}) {
    Loading.show();
    try {
        await this.$fb.deleteGuestFromGuestList(eventID, guestID);
    } catch (e) {
        showErrorMessage(e);
    }
    Loading.hide();
}
