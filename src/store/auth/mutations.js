export const setAuthState = (state, data) => {
    state.isAuthenticated = data.isAuthenticated;
    state.isReady = data.isReady;
    state.user = data.user;
};

export const setUser = (state, data) => {
    state.user = data;
};
