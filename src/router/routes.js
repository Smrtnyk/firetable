const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "/",
        name: "home",
        meta: { requiresAuth: true },
        component: () => import("pages/PageHome.vue")
      },
      {
        path: "/special-events",
        name: "specialEvents",
        meta: { requiresAuth: true },
        component: () => import("pages/Events/PageSpecialEvents.vue")
      },
      {
        path: "/events/:id",
        name: "event",
        meta: { requiresAuth: true },
        component: () => import("pages/Events/PageEvent.vue")
      },
      {
        path: "/calendar",
        name: "calendar",
        meta: { requiresAuth: true },
        component: () => import("pages/PageCalendar.vue")
      },
      {
        path: "/profile",
        name: "userProfile",
        meta: { requiresAuth: true },
        component: () => import("pages/PageProfile.vue")
      },

      // ADMIN ROUTES
      {
        path: "/admin/dashboard",
        meta: { requiresAuth: true, requiresAdmin: true },
        component: () => import("pages/Admin/PageAdminDashboard")
      },
      {
        path: "/admin/events",
        name: "adminEvents",
        meta: { requiresAuth: true, requiresAdmin: true },
        component: () => import("pages/Admin/PageAdminEvents")
      },
      {
        path: "/admin/events/:id",
        name: "adminEvent",
        meta: { requiresAuth: true, requiresAdmin: true },
        component: () => import("pages/Admin/PageAdminEvent")
      },
      {
        path: "/admin/users",
        name: "adminUsers",
        meta: { requiresAuth: true, requiresAdmin: true },
        component: () => import("pages/Admin/PageAdminUsers")
      },
      {
        path: "/admin/settings",
        name: "adminSettings",
        meta: { requiresAuth: true, requiresAdmin: true },
        component: () => import("pages/Settings/PageSettings")
      },
      {
        path: "/admin/settings/map",
        name: "adminMaps",
        meta: { requiresAuth: true, requiresAdmin: true },
        component: () => import("pages/Settings/PageMapSettings")
      },
      {
        path: "/admin/places",
        name: "adminPlaces",
        meta: { requiresAuth: true, requiresAdmin: true },
        component: () => import("pages/Admin/PageAdminPlaces")
      }
    ]
  },
  {
    path: "/auth",
    component: () => import("layouts/ClearLayout.vue"),
    children: [{ path: "/", component: () => import("pages/PageAuth.vue") }]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
