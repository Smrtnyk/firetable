import { events, guestList } from "./db";
import { uploadFile } from "./storage";
import { firestore } from "firebase/app";
const { fromDate } = firestore.Timestamp;

export async function createNewEvent (
    rootState,
    {
        eventName, eventDate, eventGuestListCount, eventImg, eventEntryPrice, map
    }
) {
    const id = events().doc().id;
    // Set the time of event to be 22h
    const timestamp = new Date(
        eventDate
            .split("/")
            .reverse()
            .join("-")
    );
    timestamp.setHours(22);
    const maps = rootState.settings.defaultMap.map(
        (defMap) => {
            return map.includes(defMap.id) && defMap;
        }
    );
    const creator = rootState.auth.user.name;
    let img = null;

    if (eventImg) {
        const url = await uploadFile({
            destination: `events/${id}`,
            base64: eventImg,
            type: "data_url"
        });
        img = url;
    }

    const eventPromise = events()
        .doc(id)
        .set({
            daily: false,
            eventName,
            eventDate: fromDate(timestamp),
            img,
            eventEntryPrice,
            eventGuestListCount,
            creator,
            pinnedMessage: null
        });

    const mapsPromises = maps.map(
        (mapPromise) => {
            return mapPromise &&
      events()
          .doc(id)
          .collection("maps")
          .doc(mapPromise.id)
          .set(mapPromise);
        }
    );

    return Promise.all([eventPromise, ...mapsPromises]);
}

/**
 * Action for adding a reservation to a table and saving it to database
 */
export function addReservation (
    rootState,
    {
        eventId, tableId, region, data
    }
) {
    const { groupedWith } = data;
    const tables = [...region.data];
    const {
        email, name, role, id
    } = rootState.auth.user;
    const reservedBy = {
        email,
        name,
        role,
        id
    };

    groupedWith.forEach((idInGroup) => {
        const findTableToReserve = tables.find((table) => {
            return table.id === idInGroup;
        });
        if (findTableToReserve) {
            findTableToReserve.reservation = {
                ...data,
                came: false,
                reservedBy
            };
        }
    });

    return events()
        .doc(eventId)
        .collection("maps")
        .doc(region.id)
        .update({ tables });
}

/**
 * Function for adding a guest doc to guestList collection for specified event.
 */
export function addGuestToGuestList (eventID, payload) {
    return guestList(eventID).add(payload);
}

/**
 * Function for deleting a guest from guestList collection for specified event.
 */
export function deleteGuestFromGuestList (eventID, guestID) {
    return guestList(eventID).doc(guestID).delete();
}

/**
 * Function for updating guest data in guestList collection for a specified event.
 */
export function updateGuestFromGuestList (eventID, guestID, payload) {
    // Something
}
