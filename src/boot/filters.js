import Vue from "vue";
import { date } from "quasar";
const { formatDate } = date;

Vue.filter("formatEventDate", (timestamp) => {
    if (!timestamp) {
        return;
    }
    const formatedTimestamp = timestamp.toDate ? timestamp.toDate() : timestamp._seconds * 1000;
    return formatDate(formatedTimestamp, "DD-MM-YYYY");
});

Vue.filter("timestampToString", (timestamp) => {
    if (!timestamp) {
        return;
    }
    return formatDate(timestamp, "DD-MM-YYYY, hh:mm");
});
